#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <qDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_8_triggered()
{
    qDebug() << "get file";
    QFileDialog *fd = new QFileDialog(this);
    fd->resize(240, 320);

    fd->setViewMode(QFileDialog::List);  //设置浏览模式，有 列表（list） 模式和 详细信息（detail）两种方式
    if (fd->exec() == QDialog::Accepted) {
        auto files = fd->selectedFiles();
        for (int i = 0;i < files.size(); ++i) {
            qDebug() << files[i];
        }
    } else fd->close();
}
