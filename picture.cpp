#include "picture.h"
#include <QString>

Picture::Picture()
{

}

Picture::Picture(QString path) :
    pic_path(path)
{

}

void Picture::setPath(QString path)
{
    pic_path = path;
}

QString Picture::getPath()
{
    return pic_path;
}

QString Picture::getFolderPath()
{
    int len = this->pic_path.length() - 1;
    while (len-- >= 0 && pic_path[len] != QChar('\\'));
    QString folderPath;
    for (int i = 0;i < len; ++i)
        folderPath.append(pic_path[i]);
    return folderPath;
}
