#ifndef PICTURE_H
#define PICTURE_H

#include <QString>

class Picture
{
    QString pic_path;

public:
    Picture();

    Picture(QString path);

    void setPath(QString path);

    QString getPath();

    QString getFolderPath();
};

#endif // PICTURE_H
